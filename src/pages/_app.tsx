import "../styles/globals.css";
import type { AppProps } from "next/app";
import type { Session } from "next-auth";
import { SessionProvider } from "next-auth/react";
import { trpc } from "../common/trpc";
import { useEffect } from "react";
import Footer from "../components/footer";
import dynamic from 'next/dynamic'
// @ts-ignore
import TawkTo from 'tawkto-react'  

interface webProps extends AppProps {
  pageProps: {
    session?: Session;
  } & AppProps["pageProps"];
}

export default trpc.withTRPC( function web ({ Component, pageProps }: webProps) {
  const Navbar = dynamic(() => import("../components/navbar"), {
    ssr: false,
    });
  useEffect(() => {
    var tawk = new TawkTo("63fb069431ebfa0fe7ef5de4", "1gq693du7")
    tawk.onStatusChange((status: string) => console.log(status))
    var removeBranding = function() {
      try {
          var element = document?.querySelector(".tawk-max-container")?.querySelector(".tawk-branding")?.parentElement
  
          if (element) {
              element.remove()
          }
      } catch (e) {}
  }
  
  var tick = 100
  
  setInterval(removeBranding, tick)
}, [])
  return (
    <SessionProvider session={pageProps.session}>
      <Navbar />
      <main className="max-w-7xl mx-auto">
      <Component {...pageProps} />
      </main>
      <Footer />
    </SessionProvider>
  );
})

// export default trpc.withTRPC(web);
