import Select from "react-select";
import { useLocalStorage } from "usehooks-ts";

export default function PricingTab() {
  const [theme, setTheme] = useLocalStorage("theme", "business");
  const kota = [{ label: "bandung", value: "bandung" },
  { label: "jawa tengah", value: "jawa tengah" },
  { label: "jawa timur", value: "jawa timur" },
  { label: "bali", value: "bali" }];
  const tarifType = [{label: "kiloan", value: "weight"}, {label: "chartered", value: "chartered"}]
  
  const customStyles = {
    control: (provided: any) => ({
      ...provided,
      backgroundColor: 'transparent',
    }),
    singleValue: (provided: any) => ({
      ...provided,
      color: theme=="business"?"#e5e7eb":"#1f2937",
    }),
    option: (provided: any) => ({
      ...provided,
      color: "#1f2937",
    }),
  };
  console.log(theme)
  return (
    <>
    <div className="form-control w-full max-w-xs">
        <label className="label">
          <span className="label-text">Jenis tarif</span>
        </label>
        <Select styles={customStyles} className="select-bordered bg-base-100 text-base-content" options={tarifType}/>
      </div>
      <div className="form-control w-full max-w-xs">
        <label className="label">
          <span className="label-text">Dari</span>
        </label>
        <Select styles={customStyles} className="select-bordered bg-base-100 text-base-content" options={kota} />
      </div>

      <div className="form-control w-full max-w-xs">
        <label className="label">
          <span className="label-text">Ke</span>
        </label>
        <Select styles={customStyles} className="select-bordered bg-base-100 text-base-content" options={kota} />
      </div>

      <div className="form-control w-full max-w-xs">
        <label className="label">
          <span className="label-text">Berat</span>
          <span className="label-text-alt">Kilogram</span>
        </label>
        <input type="number" defaultValue={1} className="input input-bordered w-full max-w-xs" />
      </div>
    </>
  );
}
