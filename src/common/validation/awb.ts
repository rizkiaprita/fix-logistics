import * as z from "zod";

export const awbCreateSchema = z.object({
  awbNumber: z.string().min(1),
});