import { NextAuthOptions, User } from "next-auth";
import Credentials from "next-auth/providers/credentials";
import { verify } from "argon2";

import { prisma } from "./prisma";
import { loginSchema } from "./validation/auth";


export const nextAuthOptions: NextAuthOptions = {
  providers: [
    Credentials({
      name: "credentials",
      credentials: {
        email: {
          label: "Email",
          type: "email",
          placeholder: "...@gmail.com",
        },
        password: { label: "Password", type: "password" },
      },
      authorize: async (credentials, req) => {
        try {
          const { email, password } = await loginSchema.parseAsync(credentials);
      
          const result: any = await prisma.user.findFirst({
            where: { email },
          });
          console.log(result)
      
          if (!result) return null;
      
          const isValidPassword = await verify(result.password, password);
      
          if (!isValidPassword) return null;
      
          return { id: result.id, email, username: result.username, role: result.role } as unknown as User;


        } catch {
          return null;
        }
      },
    }),
  ],
  callbacks: {
    jwt: async ({ token, user }) => {
      if (user) {
        token.userId = user.id;
        token.email = user.email;
        token.username = user.username;
        token.role = user.role;
      }

      return token;
    },
    session: async ({ session, token }) => {
      if (token) {
        session.user.userId = token.userId;
        session.user.email = token.email;
        session.user.username = token.username;
        session.user.role = token.role;
      }
    
      return session;
    },
  },
  session: {
    strategy: "jwt",
  },
  jwt: {
    maxAge: 15 * 24 * 30 * 60, // 15 days
  },
  pages: {
    signIn: "/login",
    newUser: "/signup",
  },
  secret: process.env.jwt_secret,
};
