/*
  Warnings:

  - The primary key for the `Kota` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The `id` column on the `Kota` table would be dropped and recreated. This will lead to data loss if there is data in the column.
  - The primary key for the `Provinsi` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The `id` column on the `Provinsi` table would be dropped and recreated. This will lead to data loss if there is data in the column.
  - Made the column `nama` on table `Kota` required. This step will fail if there are existing NULL values in that column.
  - Changed the type of `provinsiId` on the `Kota` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Made the column `nama` on table `Provinsi` required. This step will fail if there are existing NULL values in that column.

*/
-- DropForeignKey
ALTER TABLE "Kota" DROP CONSTRAINT "Kota_provinsiId_fkey";

-- AlterTable
ALTER TABLE "Kota" DROP CONSTRAINT "Kota_pkey",
DROP COLUMN "id",
ADD COLUMN     "id" SERIAL NOT NULL,
ALTER COLUMN "nama" SET NOT NULL,
DROP COLUMN "provinsiId",
ADD COLUMN     "provinsiId" INTEGER NOT NULL,
ADD CONSTRAINT "Kota_pkey" PRIMARY KEY ("id");

-- AlterTable
ALTER TABLE "Provinsi" DROP CONSTRAINT "Provinsi_pkey",
DROP COLUMN "id",
ADD COLUMN     "id" SERIAL NOT NULL,
ALTER COLUMN "nama" SET NOT NULL,
ADD CONSTRAINT "Provinsi_pkey" PRIMARY KEY ("id");

-- AlterTable
CREATE SEQUENCE user_id_seq;
ALTER TABLE "User" ALTER COLUMN "id" SET DEFAULT nextval('user_id_seq'),
ALTER COLUMN "createdAt" SET DEFAULT CURRENT_TIMESTAMP,
ALTER COLUMN "updatedAt" SET DEFAULT CURRENT_TIMESTAMP;
ALTER SEQUENCE user_id_seq OWNED BY "User"."id";

-- CreateIndex
CREATE UNIQUE INDEX "Kota_id_key" ON "Kota"("id");

-- CreateIndex
CREATE UNIQUE INDEX "Provinsi_id_key" ON "Provinsi"("id");

-- AddForeignKey
ALTER TABLE "Kota" ADD CONSTRAINT "Kota_provinsiId_fkey" FOREIGN KEY ("provinsiId") REFERENCES "Provinsi"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
