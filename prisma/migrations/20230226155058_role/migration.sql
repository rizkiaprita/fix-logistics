-- CreateEnum
CREATE TYPE "role" AS ENUM ('admin', 'staff', 'user', 'public');

-- CreateTable
CREATE TABLE "User" (
    "id" INTEGER NOT NULL,
    "username" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "role" "role" DEFAULT 'public',

    CONSTRAINT "User_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Kota" (
    "id" TEXT NOT NULL,
    "nama" TEXT,
    "provinsiId" TEXT NOT NULL,

    CONSTRAINT "Kota_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Provinsi" (
    "id" TEXT NOT NULL,
    "nama" TEXT,

    CONSTRAINT "Provinsi_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "User_username_key" ON "User"("username");

-- CreateIndex
CREATE UNIQUE INDEX "User_email_key" ON "User"("email");

-- CreateIndex
CREATE UNIQUE INDEX "Kota_id_key" ON "Kota"("id");

-- CreateIndex
CREATE UNIQUE INDEX "Provinsi_id_key" ON "Provinsi"("id");

-- AddForeignKey
ALTER TABLE "Kota" ADD CONSTRAINT "Kota_provinsiId_fkey" FOREIGN KEY ("provinsiId") REFERENCES "Provinsi"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
