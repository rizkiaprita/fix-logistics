-- CreateTable
CREATE TABLE "Awb" (
    "id" SERIAL NOT NULL,
    "awbNumber" TEXT NOT NULL,
    "senderName" TEXT NOT NULL,
    "senderCity" TEXT NOT NULL,
    "receiverName" TEXT NOT NULL,
    "receiverCity" TEXT NOT NULL,
    "status" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "Awb_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Awb_awbNumber_key" ON "Awb"("awbNumber");
